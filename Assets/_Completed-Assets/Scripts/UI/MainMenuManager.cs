using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Manages the UI in the main menu
public class MainMenuManager : MonoBehaviour
{
    private ButtonBehaviour[] playerButtons;                    // Buttons used to select the number of players
    private ButtonBehaviour[] roundsButtons;                    // Buttons used to select the number of rounds
    [SerializeField] private int maxNumberOfPlayerButtons;      // Max number of player buttons
    [SerializeField] private int maxNumberOfRoundButtons;       // Max number of round buttons
    public Color selectedColor;                                 // Color for the selected buttons
    public Color normalColor;                                   // Color for the unselected buttons
    [SerializeField] private PlayerSelection playerSelection;   // Reference to the player selection object
    

    // On Awake, finds all buttons and classifies them
    private void Awake()
    {
        Transform[] allButtons = GameObject.Find("Background").GetComponentsInChildren<Transform>();
        playerButtons = new ButtonBehaviour[maxNumberOfPlayerButtons];
        roundsButtons = new ButtonBehaviour[maxNumberOfRoundButtons];
        int i = 0;
        int j = 0;
        foreach (Transform transform in allButtons)
        {
            if (transform.GetComponent<ButtonBehaviour>())
            {
                if (transform.GetComponent<ButtonBehaviour>().getButtonType() == ButtonBehaviour.ButtonType.Players)
                {
                    playerButtons[i] = transform.GetComponent<ButtonBehaviour>();
                    i++;
                }
                if (transform.GetComponent<ButtonBehaviour>().getButtonType() == ButtonBehaviour.ButtonType.Rounds)
                {
                    roundsButtons[j] = transform.GetComponent<ButtonBehaviour>();
                    j++;
                }
            }
        }
    }

    // If a button is selected, change its color to the selected one until another button of the same Type is pressed
    public void ButtonSelected(int newNum, ButtonBehaviour.ButtonType Type)
    {
        if (Type == ButtonBehaviour.ButtonType.Players)
        {
            playerSelection.playersNumber = newNum;
            foreach (ButtonBehaviour b in playerButtons)
            {
                if (b.numberStored == newNum)
                    b.getButton().image.color = selectedColor;
                else
                    b.getButton().image.color = normalColor;
            }
        }
        else if (Type == ButtonBehaviour.ButtonType.Rounds)
        {
            playerSelection.rounsNeededToWin = newNum;
            foreach (ButtonBehaviour b in roundsButtons)
            {
                if (b.numberStored == newNum)
                    b.getButton().image.color = selectedColor;
                else
                    b.getButton().image.color = normalColor;
            }
        }
    }
}
