using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Class that manages the behaciour of the buttons
public class ButtonBehaviour : MonoBehaviour, IPointerClickHandler
{
    public enum ButtonType { Players, Start, Exit, Rounds } // possible button types
    private Button button;                                  // Reference to the button that calls this script
    private MainMenuManager manager;                        // Reference to the Main Menu Manager
    [SerializeField] private ButtonType Type;               // Type instance from the enum
    public AudioSource source;                              // Reference to the Audio Source
    public int numberStored;                                // Value stored inside of the button

    // Returns the Button Type from the enum selected for this button
    public ButtonType getButtonType()
    {
        return Type;
    }

    // Returns the Button from the UI
    public Button getButton()
    {
        return button;
    }

    // Called when the object awakes
    private void Awake()
    {
        // Gets the Button and Audio Source attached to the same Game Object and looks in the scene for the Main Menu Manager
        button = GetComponent<Button>();
        source = GetComponent<AudioSource>();
        manager = GameObject.Find("MainMenuManager").GetComponent<MainMenuManager>();
    }

    // Method called upon clicking the button, changes its behaviour according to its ButtonType
    public void OnPointerClick(PointerEventData eventData) 
    {
        source.Play();
        if (Type == ButtonType.Exit)
        {
            Application.Quit();
        }
        if (Type == ButtonType.Players)
        {
            manager.ButtonSelected(numberStored, Type);
        }
        if (Type == ButtonType.Rounds)
        {
            manager.ButtonSelected(numberStored, Type);
        }
        if (Type == ButtonType.Start)
        {
            SceneManager.LoadScene("_Complete-Game");
        }
    }
}
