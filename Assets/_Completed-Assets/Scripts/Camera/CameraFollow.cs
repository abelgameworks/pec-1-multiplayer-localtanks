using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script for making the camera follow the player
public class CameraFollow : MonoBehaviour
{
    private Camera m_Camera; // Reference to the camera
    public float smooth = 0.5f; // Smoothnes for the movement
    public float limitDist = 20.0f; // Limit of distance the tank can displace itself without the camera moving

    private void FixedUpdate()
    {
        if (m_Camera == null)
        {
            m_Camera = GetComponentInChildren<Camera>(); // if there is no camera assigned, looks for the one for the corresponding tank
            return;
        }

        Follow();
    }

    // Script for following the player
    private void Follow()
    {
        // Calculates the distance to the player
        float currentDist = Vector3.Distance(transform.position, m_Camera.transform.position);

        // If the distance is bigger than the limit, move the camera smoothly
        if(currentDist > limitDist)
            m_Camera.transform.position = Vector3.Lerp(m_Camera.transform.position, transform.position, Time.deltaTime * smooth);
    }

}
