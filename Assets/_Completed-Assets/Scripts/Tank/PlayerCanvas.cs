using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// Script to control the canvas related to each player
public class PlayerCanvas : MonoBehaviour
{
    public TextMeshProUGUI RoundsWon;           // Text showing how many rounds the player won
    public Scrollbar Health;                    // Scrollbar showing the remaining health of the player
    public GameObject PlayerPanel;              // Reference to the panel containing the player information
    public GameObject PlayerNotJoined;          // Reference to the message offering the player to join
    private bool joined = false;                // Checks if the player has joined the game or not
    private float timer;                        // Timer for the blinking effect

    // Changes the joined status and shows the PlayerPanel or the PlayerNotJoined accordingly
    public void SetActive(bool active)
    {
        if (joined)
            PlayerPanel.SetActive(active);
        else
            PlayerNotJoined.SetActive(active);
    }

    private void Update()
    {
        // If the player didn't join, make the message blink
        if (!joined)
        {
            timer = timer + Time.deltaTime;
            BlinkText();
        }

    }

    // Makes the text blink each half second
    private void BlinkText()
    {
        if (timer >= 0.5)
            PlayerNotJoined.SetActive(false);
        if (timer >= 1)
        {
            PlayerNotJoined.SetActive(true);
            timer = 0;
        }
    }

    // Changes the value shown for the rounds won
    public void SetRoundsWon(int rounds)
    {
        RoundsWon.text = rounds.ToString();
    }

    // Changes the value shown for the remaining health
    public void SetRemainingHealth(float health)
    {
        Health.size = health;
    }

    // Changes the value of the joined check
    public void SetJoined(bool value)
    {
        joined = value;

        PlayerNotJoined.SetActive(!joined);
        PlayerPanel.SetActive(joined);

    }
}
