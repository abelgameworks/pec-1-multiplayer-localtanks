using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

// Script to catch the events from the controller, keyboard was deleted due to problems reading the movement and the rotation
// This script is currently not in use
public class TankActions : MonoBehaviour
{
    private TankControl control;                // Reference to the tank control script generated from the input
    private InputAction movement;               // Reference to the movement action in the controller
    private InputAction rotation;               // Reference to the rotation action in the controller

    public enum ShootStatus { Starting, Loading, Fire, None }           // Enum with the different Shoot Statuses
    public ShootStatus shootStatus;                                     // Status of the normal Shoot
    public ShootStatus superShootStatus;                                // Status of the super Shoot
    public bool started;                                                // Control boolean to know if the tank is playing or not

    private void Awake()
    {
        // Creates a new Instance of the Tank Control class and sets the status of both shoots to None
        control = new TankControl();
        shootStatus = ShootStatus.None;
        superShootStatus = ShootStatus.None;
    }

    // On the enable class, the events get registered and enabled
    private void OnEnable()
    {
        movement = control.Controller.Move;
        movement.Enable();

        rotation = control.Controller.Rotate;
        rotation.Enable();

        control.Controller.Shoot.started += DoShoot;
        control.Controller.Shoot.performed += DoShoot;
        control.Controller.Shoot.canceled += DoShoot;
        control.Controller.Shoot.Enable();

        control.Controller.SuperShoot.started += DoSuperShoot;
        control.Controller.SuperShoot.performed += DoSuperShoot;
        control.Controller.SuperShoot.canceled += DoSuperShoot;
        control.Controller.SuperShoot.Enable();

        control.Controller.StartButton.started += DoStartButton;
        control.Controller.StartButton.Enable();
    }

    // Method called when the event of pressing the Start button happens
    private void DoStartButton(InputAction.CallbackContext obj)
    {
        if (obj.started && !started)
            started = true;
    }

    // Method called when the event of pressing the Shoot button happens
    // Value change if the button just got pressed, it was already pressed or if it has just been released
    private void DoShoot(InputAction.CallbackContext obj)
    {
        if (obj.started)
            shootStatus = ShootStatus.Starting;
        if (obj.performed)
            shootStatus = ShootStatus.Loading;
        if (obj.canceled)
            shootStatus = ShootStatus.Fire;
    }

    // Method called when the event of pressing the SuperShoot button happens
    // Value change if the button just got pressed, it was already pressed or if it has just been released
    private void DoSuperShoot(InputAction.CallbackContext obj)
    {
        if (obj.started)
            superShootStatus = ShootStatus.Starting;
        if (obj.performed)
            superShootStatus = ShootStatus.Loading;
        if (obj.canceled)
            superShootStatus = ShootStatus.Fire;
    }

    // Disables the event reading if the object gets disabled
    private void OnDisable()
    {
        movement.Disable();
        rotation.Disable();
        control.Controller.Shoot.Disable();
    }

    // Reads the vector 2 from the movement stick/buttons and calculates its magnitude.
    // If one of the axis is negative, the value gets multiplied by -1
    public float GetMovementValue()
    {
        Vector2 input = movement.ReadValue<Vector2>();
        if (input.x > 0 && input.y > 0)
            return input.magnitude;
        else if (input.x > 0 && input.y < 0)
            return input.magnitude;
        else if (input.x < 0 && input.y < 0)
            return input.magnitude * -1;
        else if (input.x < 0 && input.y > 0)
            return input.magnitude * -1;
        else
            return 0f;
    }

    // Reads the vector 2 from the rotation stick/buttons and calculates its magnitude.
    // If one of the axis is negative, the value gets multiplied by -1
    public float GetRotationValue()
    {
        Vector2 input = rotation.ReadValue<Vector2>();
        if (input.x > 0 && input.y > 0)
            return input.magnitude;
        else if (input.x > 0 && input.y < 0)
            return input.magnitude;
        else if (input.x < 0 && input.y < 0)
            return input.magnitude * -1;
        else if (input.x < 0 && input.y > 0)
            return input.magnitude * -1;
        else
            return 0f;
    }   
}


