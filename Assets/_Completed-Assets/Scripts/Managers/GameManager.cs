using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Cinemachine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

namespace Complete
{
    public class GameManager : MonoBehaviour
    {
        public int m_NumRoundsToWin = 5;            // The number of rounds a single player has to win to win the game.
        public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases.
        public float m_EndDelay = 3f;               // The delay between the end of RoundPlaying and RoundEnding phases.
        public CameraControl m_CameraControl;       // Reference to the CameraControl script for control during different phases.
        public Text m_MessageText;                  // Reference to the overlay Text to display winning text, etc.
        public Text m_SubMessageText;                  // Reference to the overlay Text to display winning text, etc.
        public GameObject m_TankPrefab;             // Reference to the prefab the players will control.
        public TankManager[] m_Tanks;               // A collection of managers for enabling and disabling different aspects of the tanks.
        
        private int m_RoundNumber;                  // Which round the game is currently on.
        private WaitForSeconds m_StartWait;         // Used to have a delay whilst the round starts.
        private WaitForSeconds m_EndWait;           // Used to have a delay whilst the round or game ends.
        private TankManager m_RoundWinner;          // Reference to the winner of the current round.  Used to make an announcement of who won.
        private TankManager m_GameWinner;           // Reference to the winner of the game.  Used to make an announcement of who won.

        private int playerNum;                      // Number of players that joined
        private int remainingPlayers;               // Number of players still playing
        public bool loaded = false;                 // Checks if the script has fully loaded the game
        private Camera mainCam;                     // Reference to the main camera to initialize the others
        private float ortographicSize;              // Reference to the size for the camera
        private PlayerSelection playerSelection;    // Reference to the object PlayerSelection from the DontDestroy context

        private void Start()
        {
            // Create the delays so they only have to be made once.
            m_StartWait = new WaitForSeconds (m_StartDelay);
            m_EndWait = new WaitForSeconds (m_EndDelay);

            // Looks for the PlayerSelection Object and reads its values
            playerSelection = GameObject.FindGameObjectWithTag("PlayerSelection").GetComponent<PlayerSelection>();
            playerNum = playerSelection.playersNumber;
            m_NumRoundsToWin = playerSelection.rounsNeededToWin;

            remainingPlayers = playerNum;

            SpawnAllTanks();
            SetCameraTargets();

            // Let the rest of the objects know that the manager is fully loaded
            loaded = true;

            // Once the tanks have been created and the camera is using them as targets, start the game.
            StartCoroutine (GameLoop ());
        }

        // Returns the number of players in the game
        public int getPlayerNum()
        {
            return playerNum;
        }

        // Returns the number of players in the game that are still alive
        public int getRemainingPlayers()
        {
            return remainingPlayers;
        }

        // Method for Spawning the tanks needed and for leaving the rest ready to join the game
        private void SpawnAllTanks()
        {
            // Looks for the main camera and saves a reference to it and stores in another variable its ortographic size value
            mainCam = GameObject.Find("Main Camera").GetComponent<Camera>();
            ortographicSize = mainCam.orthographicSize; // Sometimes the ortographic size of the main cam gets changed so we store it before that happens

            // For all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // ... create them, set their player number and references needed for control.
                m_Tanks[i].m_Instance = 
                    Instantiate(m_TankPrefab, m_Tanks[i].m_SpawnPoint.position, m_Tanks[i].m_SpawnPoint.rotation) as GameObject;
                m_Tanks[i].m_PlayerNumber = i + 1;
                m_Tanks[i].m_Instance.GetComponent<TankHealth>().playerNum = i + 1;
                if (i < playerNum)
                {
                    m_Tanks[i].Setup();
                    m_Tanks[i].alive = true;
                    m_Tanks[i].m_Shooting.active = true;
                    m_Tanks[i].m_Movement.active = true;
                    AddCamera(i, 0);
                }
                else
                {
                    m_Tanks[i].m_PlayerNumber = i + 1;
                    m_Tanks[i].SetupUnused();
                }
            }
            // Deactivate the main camera since each tank has its own
            mainCam.gameObject.SetActive(false);
        }

        // Method for joining a new tank to the game
        public void SpawnNewTank(int player)
        {
            // Checks that no player has already died, if it did, the player wont be able to join the game until next round
            if (remainingPlayers == playerNum)
            {
                // Increase the number of players and the number of players still playing
                playerNum+=1;
                remainingPlayers+=1;

                // For each player, set the new camera values
                for (int i = 0; i < playerNum+1; i++)
                {
                    AddCamera(i, player);
                }

                // Let the player control the tank
                m_Tanks[player - 1].m_Shooting.active = true;
                m_Tanks[player - 1].m_Movement.active = true;
                m_Tanks[player - 1].alive = true;
                m_Tanks[player - 1].StartUnused();
            }
        }

        // Boolean used to set the two player view
        private bool firstCamera = true;
        private void AddCamera(int i, int player)
        {
            if (i < playerNum)
            {
                Camera newCam;

                //Special case used when the third player joining is player 4
                if (player == 4 && i == 2)
                    i = 3;

                // If the tank has no Camera, add a new one to the object. It also sets the cullingMask of the camera and adds a CinemachineBrain to it
                // if it already has a camera, get it from the reference in the TankManager
                if (!m_Tanks[i].m_Instance.transform.Find("Camera" + m_Tanks[i].m_PlayerNumber))
                {
                    GameObject childCam = new GameObject("Camera" + m_Tanks[i].m_PlayerNumber);
                    newCam = childCam.AddComponent<Camera>();

                    m_Tanks[i].m_Instance.transform.Find("FollowCam").gameObject.layer = LayerMask.NameToLayer("Camera" + m_Tanks[i].m_PlayerNumber);

                    childCam.transform.parent = m_Tanks[i].m_Instance.transform;

                    childCam.AddComponent<CinemachineBrain>();
                    newCam.cullingMask += LayerMask.NameToLayer("Camera" + m_Tanks[i].m_PlayerNumber);
                    newCam.CopyFrom(mainCam);
                    newCam.orthographicSize = ortographicSize;
                    m_Tanks[i].cameraReference = newCam;
                }
                else
                {
                    newCam = m_Tanks[i].cameraReference;
                }
                // If there are only two players in the game
                if (playerNum == 2 || remainingPlayers == 2)
                {
                    if (firstCamera)
                    {
                        newCam.rect = new Rect(0.0f, 0.5f, 1.0f, 0.5f);
                    }
                    else
                    {
                        newCam.rect = new Rect(0.0f, 0.0f, 1.0f, 0.5f);
                        GameObject.Find("MinimapCam").GetComponent<Camera>().rect = new Rect(0f, 0f, 0f, 0f);
                    }
                    firstCamera = !firstCamera;
                }
                else
                {
                    if (i == 0)
                        newCam.rect = new Rect(0.0f, 0.5f, 0.5f, 0.5f);
                    if (i == 1)
                        newCam.rect = new Rect(0.0f, 0.0f, 0.5f, 0.5f); 
                    // If there are 3 players
                    if (playerNum == 3)
                    {
                        // Checks if the player trying to join is player 3 or player 4
                        if (i == 2)
                        {
                            newCam.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                            GameObject.Find("MinimapCam").GetComponent<Camera>().rect = new Rect(0.5f, 0.0f, 0.5f, 0.5f);
                        }
                        else  if (i == 3)
                        {
                            newCam.rect = new Rect(0.5f, 0.0f, 0.5f, 0.5f);
                            GameObject.Find("MinimapCam").GetComponent<Camera>().rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                        }
                    }        
                    // If there are 4 players
                    if (playerNum == 4)
                    {
                        // Checks if the player trying to join is player 3 or player 4
                        if (i == 2)
                        {
                            newCam.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                            GameObject.Find("MinimapCam").GetComponent<Camera>().rect = new Rect(0f, 0f, 0f, 0f);
                        }
                        else if (i == 3)
                        {
                            newCam.rect = new Rect(0.5f, 0.0f, 0.5f, 0.5f);
                            GameObject.Find("MinimapCam").GetComponent<Camera>().rect = new Rect(0f, 0f, 0f, 0f);
                        }
                    }
                }
            }
        }

        private void SetCameraTargets()
        {
            // Create a collection of transforms the same size as the number of tanks.
            Transform[] targets = new Transform[m_Tanks.Length];

            // For each of these transforms...
            for (int i = 0; i < playerNum; i++)
            {
                // ... set it to the appropriate tank transform.
                targets[i] = m_Tanks[i].m_Instance.transform;
            }   

            // These are the targets the camera should follow.
            m_CameraControl.m_Targets = targets;
        }


        // This is called from start and will run each phase of the game one after another.
        private IEnumerator GameLoop ()
        {
            // Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
            yield return StartCoroutine (RoundStarting ());

            // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
            yield return StartCoroutine (RoundPlaying());

            // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished.
            yield return StartCoroutine (RoundEnding());

            // This code is not run until 'RoundEnding' has finished.  At which point, check if a game winner has been found.
            if (m_GameWinner != null)
            {
                // If there is a game winner, restart the level if the number of wins is less than the goal.
                SceneManager.LoadScene("Main Menu");
            }
            else
            {
                // If there isn't a winner yet, restart this coroutine so the loop continues.
                // Note that this coroutine doesn't yield.  This means that the current version of the GameLoop will end.
                StartCoroutine (GameLoop ());
            }
        }

        private IEnumerator RoundStarting ()
        {
            // As soon as the round starts reset the tanks and make sure they can't move.
            ResetAllTanks ();
            DisableTankControl ();

            for (int i = 0; i < m_Tanks.Length; i++)
            {
                m_Tanks[i].canvas.SetActive(true);
            }

            // Snap the camera's zoom and position to something appropriate for the reset tanks.
            m_CameraControl.SetStartPositionAndSize ();

            // Increment the round number and display text showing the players what round it is.
            m_RoundNumber++;
            m_MessageText.text = "ROUND " + m_RoundNumber;

            // Wait for the specified length of time until yielding control back to the game loop.
            yield return m_StartWait;
        }


        private IEnumerator RoundPlaying ()
        {
            // As soon as the round begins playing let the players control the tanks.
            EnableTankControl ();

            // Clear the text from the screen.
            m_MessageText.text = string.Empty;
            m_SubMessageText.text = string.Empty;

            // While there is not one tank left...
            while (!OneTankLeft())
            {
                // ... return on the next frame.
                yield return null;
            }
        }

        // Public method to get the tanks that are still alive
        public TankManager[] GetRemainingTanks()
        {
            TankManager[] activeTanks = new TankManager[remainingPlayers];
            int j = 0;
            for (int i = 0; i < playerNum; i++)
            {
                if (m_Tanks[i].alive)
                {
                    activeTanks[j] = m_Tanks[i];
                    j++;
                }
            }

            return activeTanks;
        }

        // Method called when a player health reaches 0
        public void PlayerDeath(int player)
        {
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                if (m_Tanks[i].m_PlayerNumber == player)
                {
                    m_Tanks[i].alive = false;
                }
            }
            ReadjustCameras();
        }

        // After a tank dies, this method calculates the remaining player and sets the corresponding camera views
        private void ReadjustCameras()
        {
            TankManager[] activeTanks = new TankManager[remainingPlayers];
            TankManager[] deactivedTanks = new TankManager[remainingPlayers];
            int j = 0;
            int k = 0;
            for (int i = 0; i < playerNum; i++)
            {
                if (m_Tanks[i].alive)
                {
                    activeTanks[j] = m_Tanks[i];
                    activeTanks[j].cameraReference = m_Tanks[i].cameraReference;
                    j++;
                }
                else
                {
                    deactivedTanks[k] = m_Tanks[i];
                    deactivedTanks[k].cameraReference = m_Tanks[i].cameraReference;
                    k++;
                }
            }

            remainingPlayers = j;

            if (j == 3 && deactivedTanks[0] != null)
                PlaceMinimap(deactivedTanks[0]);

            if (j == 2)
            {
                for (int i = 0; i < j; i++)
                {
                    AddCamera(activeTanks[i].m_PlayerNumber-1, 0);
                }
            }
        }

        // Places the minimap on the position of the given tank camera
        private void PlaceMinimap(TankManager tank)
        {
            GameObject.Find("MinimapCam").GetComponent<Camera>().rect = tank.cameraReference.rect;
            tank.m_Instance.SetActive(false);
        }

        private IEnumerator RoundEnding ()
        {
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                m_Tanks[i].canvas.SetActive(false);
            }
            // Stop tanks from moving.
            DisableTankControl ();

            // Clear the winner from the previous round.
            m_RoundWinner = null;

            // See if there is a winner now the round is over.
            m_RoundWinner = GetRoundWinner ();

            // If there is a winner, increment their score.
            if (m_RoundWinner != null)
            {
                m_RoundWinner.m_Wins++;
                m_RoundWinner.canvas.SetRoundsWon(m_RoundWinner.m_Wins);
            }

            // Now the winner's score has been incremented, see if someone has one the game.
            m_GameWinner = GetGameWinner ();

            // Get a message based on the scores and whether or not there is a game winner and display it.
            string[] message = EndMessage ();
            m_MessageText.text = message[0];
            m_SubMessageText.text = message[1];

            // Wait for the specified length of time until yielding control back to the game loop.
            yield return m_EndWait;
        }


        // This is used to check if there is one or fewer tanks remaining and thus the round should end.
        private bool OneTankLeft()
        {
            // Start the count of tanks left at zero.
            int numTanksLeft = 0;

            // Go through all the tanks...
            for (int i = 0; i < playerNum; i++)
            {
                // ... and if they are active, increment the counter.
                if (m_Tanks[i].alive)
                    numTanksLeft++;
            }

            remainingPlayers = numTanksLeft;
            // If there are one or fewer tanks remaining return true, otherwise return false.
            return numTanksLeft <= 1;
        }
        
        
        // This function is to find out if there is a winner of the round.
        // This function is called with the assumption that 1 or fewer tanks are currently active.
        private TankManager GetRoundWinner()
        {
            // Go through all the tanks...
            for (int i = 0; i < playerNum; i++)
            {
                // ... and if one of them is active, it is the winner so return it.
                if (m_Tanks[i].m_Instance.activeSelf)
                    return m_Tanks[i];
            }

            // If none of the tanks are active it is a draw so return null.
            return null;
        }


        // This function is to find out if there is a winner of the game.
        private TankManager GetGameWinner()
        {
            // Go through all the tanks...
            for (int i = 0; i < playerNum; i++)
            {
                // ... and if one of them has enough rounds to win the game, return it.
                if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
                {
                    playerSelection.gamesWon[m_Tanks[i].m_PlayerNumber]++;
                    return m_Tanks[i];
                }
            }

            // If no tanks have enough rounds to win, return null.
            return null;
        }


        // Returns a string message to display at the end of each round.
        private string[] EndMessage()
        {
            string[] messages = new string[2];
            // By default when a round ends there are no winners so the default end message is a draw.
            messages[0] = "DRAW!";
            messages[1] = string.Empty;

            // If there is a winner then change the message to reflect that.
            if (m_RoundWinner != null)
            {
                messages[0] = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";

                // Add some line breaks after the initial message.
                messages[0] += "\n\n\n\n";

                // Go through all the tanks and add each of their scores to the message.
                for (int i = 0; i < playerNum; i++)
                {
                    messages[0] += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
                }
            }
            // If there is a game winner, change the entire message to reflect that.
            if (m_GameWinner != null)
            {
                messages[0] = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";
                messages[0] += "\n \n \n";

                // Go through all the tanks and add each of their scores to the message.
                for (int i = 1; i < playerNum; i++)
                {
                    if (m_Tanks[i].m_PlayerNumber == m_GameWinner.m_PlayerNumber)
                        messages[0] += m_GameWinner.m_ColoredPlayerText + " HAS " + playerSelection.gamesWon[i] + " GAMES WON! ";
                    else    
                        messages[1] += m_Tanks[i].m_ColoredPlayerText +": " + playerSelection.gamesWon[i] + " GAMES WON\n";
                }
            }

            return messages;
        }


        // This function is used to turn all the tanks back on and reset their positions and properties.
        private void ResetAllTanks()
        {
            for (int i = 0; i < playerNum; i++)
            {
                m_Tanks[i].Reset();
            }
        }


        private void EnableTankControl()
        {
            for (int i = 0; i < playerNum; i++)
            {
                m_Tanks[i].EnableControl();
            }
        }


        private void DisableTankControl()
        {
            for (int i = 0; i < playerNum; i++)
            {
                m_Tanks[i].DisableControl();
            }
        }
    }
}