using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerSelection : MonoBehaviour
{
    public int playersNumber = 0;
    public int rounsNeededToWin = 0;
    public int[] gamesWon;

    private void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("PlayerSelection");

        if (objs.Length > 1)
            Destroy(this.gameObject);

        gamesWon = new int[4];
        for (int i = 0; i < gamesWon.Length; i++)
        {
            gamesWon[i] = 0;
        }

        DontDestroyOnLoad(this.gameObject);
    }
}
