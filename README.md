﻿PEC 1 
-
 Multijugador local

Abel Pazó Otero

________________


Tareas Obligatorias


Todas las tareas se han realizado a partir del proyecto resultante de realizar los retos. Para una información más al detalle, consultar los comentarios de los scripts.
Selección de jugadores y habilitación de cámaras
        Tras la realización de los retos establecidos, se inició el proceso de ampliar el multijugador hasta 4 jugadores. Cambios realizados:


* GameManager


En primer lugar, se modifica el gameobject del Game Manager, añadiendo dos nuevos spawn points para los nuevos jugadores, pasando a ser el jugador 3 el jugador verde y el 4 el amarillo. 


Se modifican los procesos de inicializar las cámaras, inicializando las de todos los tanques pero sólo asignando espacio en pantalla a las cámaras de los tanques activos en este momento.


* Tank Manager


Se crea un método llamado SetupUnused() y otro llamado StartUnused(). El primero es utilizado para inicializar los tanques de los jugadores que no se han unido a la partida y el segundo para activar el tanque correspondiente a un jugador que desee unirse al juego.


* Camera Control


Se modifica la comprobación para activar la cámara única cuando solo quedan dos tanques restantes y estos están cerca. La modificación comprueba que solo queden dos jugadores jugando en esta ronda y la distancia entre éstos.


* Main Menu Manager


Se crea una pantalla de menú inicial desde la que elegir tanto el número de jugadores que van a participar como el número de rondas que se jugarán.


* Button Behaviour


Se crea un script para controlar el comportamiento de los botones añadidos al menú principal, pudiendo estos modificar el número de jugadores/rondas, salir de la aplicación o inicializar el juego.


________________


* Player Selection


Este script replica el patrón Singleton para que solo haya una instancia suya por ejecución del juego. En el script se almacenan el número de jugadores seleccionados para empezar la siguiente partida, el número de rondas que se jugarán y el número de partidas ganadas por cada jugador. Todos estos datos serán usados después por el Game Manager para inicializar la partida, a excepción de las partidas ganadas por jugador, que se incrementará y mostrará al final de cada partida.
Unión de jugadores durante la partida
* Tank Shooting


Se habilita un botón adicional codificado como “Start” y utilizado para unirse a la partida.


* Game Manager


Se crea un nuevo método al que se llama cuando un jugador que no se ha unido a la partida pulsa el botón “Start”. Este método también llamará al método que setea las cámaras para que aparezca la pantalla correspondiente a cada jugador. A mayores, se incrementa el número de jugadores tanto en la partida como restantes.
Fin de partida y vuelta al inicio
* Game Manager


En el Game Manager ya se controlaba el fin de partida cuando se llegaban a las 5 rondas, la modificación para esta tarea ha sido que el número de rondas ya no está fijado en el script, sino que se recoge del objeto Player Selection (procedente del menú principal). Si un jugador alcanza el número de rondas ganadas marcadas como objetivo, se volverá al menú principal tras mostrar los resultados de partidas ganadas.


Tareas Opcionales
Minimapa


* Game Manager


 Se modifica la posición y tamaño del mapa en función de los jugadores restantes. Si hay 2 o 4 jugadores, el rect de la cámara será 0, mientras que si hay 3 jugadores se mostrará en la pantalla libre. Si hay 3 jugadores unidos, el minimapa se mostrará en la posición del jugador 3 o jugador 4; mientras que si hay 4 jugadores y uno se queda sin salud, el minimapa se mostrará en el espacio correspondiente a dicho jugador.
Cinemachine


* Tank Prefab


Se crea un nuevo hijo dentro del Prefab del tanque llamado FollowCam. Este hijo posee un componente CineMachineVirtualCam configurado para seguir al tanque correspondiente.


* Game Manager


Se setea la LayerMask de la cámara para que corresponda con su Layer. Al objeto Cámara que se crea durante la creación de los tanques se le añade un CinemachineBrain para gestionar la CineMachineVirtualCam. A este objeto cámara se le añade el Layer seteado anteriormente al CullingMask para asegurar el correcto funcionamiento de las diferentes cámaras.
New Input System


* Tank Control


Se han creado los Action Maps para mando y teclado y a partir de éstas se ha generado su script para poder programar a través de eventos. Se han definido las Actions para Move (acción de tipo Value y control de tipo Stick), Rotate (acción de tipo Value y control de tipo Stick), StartButton, Shoot y SuperShoot ( los tres con acción de tipo Value y control de tipo Stick, siendo la diferencia que Shoot y SuperShoot tienen un trigger behaviour de Press and Release y StartButton de Press Only), 


* Tank Actions


Script que lee los cambios en el input que use el jugador. Este script se encarga de leer los valores de desplazamiento y rotación y si se ha pulsado el botón de “Shoot” o “SuperShoot”


* Tank Movement


Se modifica el script para que los valores leídos para realizar el movimiento o la rotación sean los leídos por la instancia de Tank Actions correspondiente al tanque. Para ello se lee el valor en vector2 directamente del stick o las teclas definidas. Una vez se tiene el vector2, se calcula su magnitud y se multiplica por -1 si alguno de los ejes tiene valor negativo.


* Tank Shooting


Se modifica el script para que los valores leídos para realizar el “Shoot” o el “Super Shoot”  sean los leídos por la instancia de Tank Actions correspondiente al tanque. En este caso, para el inicio de la carga, la carga y el disparo; se han utilizado los eventos “started”, “performed” y “canceled” de un botón con comportamiento “Press and Release”


* Importante


A pesar de haber utilizado muchos acercamientos diferentes a este tema, no se ha conseguido hacer funcionar correctamente el New Input System, en el Game Manager se ha intentado instanciar todo de forma individual e incluso vincular cada controlador con un jugador distinto, pero aún así cada vez que se movía un Stick se movían todos los tanques. A la hora de disparar este problema no existía, disparando el tanque que corresponde.
Posibles Mejoras
Menú de inicio y selector de jugadores


        Ya realizado durante las tareas “Selección de jugadores”, “Habilitación de cámaras” y “Minimapa”
Pantalla dividida dinámica y comportamiento de cámara huérfana
        Ya realizado durante las tareas “Selección de jugadores”, “Habilitación de cámaras” y “Minimapa”
Fin de partida y reinicio del juego
        Ya realizado durante la tarea “Fin de partida y vuelta al inicio”.
Interfaz gráfica


* Player Canvas


Script para controlar los elementos del canvas propios para cada jugador. Cada jugador posee un mensaje de “Press Start to Join” parpadeante hasta que se unan; si se unen, este se reemplaza por un panel en el que aparecen el número y color del jugador, su salud actual y las rondas ganadas en esta partida.


Una vez el jugador se une a la partida, los diferentes elementos del canvas serán controlados por los managers para representar la información correspondiente.


* Main Menu Manager


Se ha realizado un método para que el botón seleccionado tanto para players como para rondas aparezca resaltado hasta que se elija otro de la misma categoría.
        


Enlaces
https://gitlab.com/abelgameworks/pec-1-multiplayer-localtanks 
Créditos


“The Tread of War”
Composed by Jonathan Shaw
(www.jshaw.co.uk )